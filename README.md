# Rsranger65/dotfiles
My personal collection of dotfiles I use to configure every Linux/MacOS system I touch.

## Quickstart

	#Verify that git, zsh, and pkgfile are installed
	sudo pkgfile --update    # If not done already
	cd ~
    git clone https://github.com/win93/dotfiles .dotfiles
	.dotfiles/home/install.sh
	chsh -s /bin/zsh

`install.sh` runs `git pull` internally, so all you need to do to update is run `install.sh` again.
