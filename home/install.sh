#!/bin/bash

dir="$(dirname "$0")"
#reponame,install-loc[, "../" one or more times to get the symbolic link back to the cwd]
files="gitconfig,.gitconfig grml.zsh,.grml.zsh zshrc,.zshrc htoprc,.config/htop/htoprc,../../ tmux.conf,.tmux.conf vimrc,.vimrc"

git -C "$dir" fetch
if ! git -C "$dir" diff --quiet -- remotes/origin/HEAD; then
	git -C "$dir" pull
	exec "$0" #Be paranoid and re-exec the script in case pulling changed it
fi

if [ $PWD != $HOME ]; then
	printf "==============================================================\n"
	printf "WARN: The current working directory is not your home directory\n"
	printf "==============================================================\n"
	printf "\n"
fi

printf "The following files will be added/replaced with symbolic links:\n"
for file in $files; do
	to="$PWD/$(echo "$file" | cut -d, -f2)"
	if [ -L "$to" ]; then
		status="relink"
	elif [ -f "$to" ]; then
		status="overwrite"
	else
		status="add"
	fi
	printf "  %9s %s\n" "$status" "$to"
done
printf "\n"
read -p "Press enter to continue or ^C to abort "

grml_url="https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc"
printf "Fetching grml-zsh-config from %s into %s\n" "$grml_url" "$dir/grml.zsh"
curl -L -o "$dir/grml.zsh" "$grml_url" || exit $?

for file in $files; do
	from="$(echo "$file" | cut -d, -f3)$dir/$(echo "$file" | cut -d, -f1)"
	to="$PWD/$(echo "$file" | cut -d, -f2)"
	mkdir -pv "$(dirname "$to")"
	ln -sfv "$from" "$to"
done
